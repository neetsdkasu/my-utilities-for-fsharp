//------------------------------------
// Seq Extensions
//
// version: 1.0
// author : Leonardone @ NEETSDKASU
// license: MIT License
//------------------------------------
namespace neetsdkasu.utils.Collections


module SeqEx =
    
    //
    // cycling sequence infinity
    //
    // val cycle : seq<'a> -> seq<'a>
    //
    let cycle src =
        if Seq.isEmpty src then
            invalidArg "src" "sequence is empty"
        else
            Seq.unfold (fun tmp ->
                if Seq.isEmpty tmp
                then Some(Seq.head src, Seq.skip 1 src)
                else Some(Seq.head tmp, Seq.skip 1 tmp)
             ) src
    
    
    //
    // Seq.skip 1
    //
    // val tail : seq<'a> -> seq<'a>
    //
    let tail s = Seq.skip 1 s
    