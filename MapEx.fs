//------------------------------------
// Map Extensions
//
// version: 1.0
// author : Leonardone @ NEETSDKASU
// license: MIT License
//------------------------------------
namespace neetsdkasu.utils.Collections


module MapEx =
    
    //
    // find value (if key is not found, return default value)
    //
    // val findWithDefault : 'a -> 'b -> Map<'b,'a> -> 'a when 'b : comparison
    //
    let findWithDefault defaultValue key map =
        match Map.tryFind key map with
        | Some(v) -> v
        | None    -> defaultValue
    
    
    //
    // insert value (if key is existed, update value with existed value)
    //
    // val insertWith :
    //  ('a -> 'a -> 'a) -> 'b -> 'a -> Map<'b,'a> -> Map<'b,'a>
    //    when 'b : comparison
    //
    let insertWith update key value map =
        let newValue =
            match Map.tryFind key map with
            | Some(p) -> update value p
            | None    -> value
        in Map.add key newValue map

    
    //
    // adjust: update value
    //
    // val adjust :
    //  ('a -> 'a) -> 'b -> Map<'b,'a> -> Map<'b,'a> when 'b : comparison
    //
    let adjust update key map =
        match Map.tryFind key map with
        | Some(v) -> Map.add key (update v) map
        | None    -> map

