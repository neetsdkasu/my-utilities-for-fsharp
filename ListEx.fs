//------------------------------------
// List Extensions
//
// version: 1.0
// author : Leonardone @ NEETSDKASU
// license: MIT License
//------------------------------------
namespace neetsdkasu.utils.Collections

module ListEx =
    
    //
    // take list tail (if list is empty, return empty list)
    //
    // val tail : 'a list -> 'a list
    //
    let tail =
        function 
        | []    -> []
        | _::ls -> ls

