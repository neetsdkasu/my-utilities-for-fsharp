//------------------------------------
// Test of Map Extensions
//
// version: 1.0
// author : Leonardone @ NEETSDKASU
// license: MIT License
//------------------------------------
#load "TUnit.fsx"
#load "../MapEx.fs"

open neetsdkasu.utils.Collections


do  TUnit.newTest "MapEx.findWithDefault"
    |> TUnit.addCase (3, 1, Map.ofList [(1, 10);(4, 99);(11, 4)]) 10
    |> TUnit.addCase (3, 1, Map.ofList [(2, 10);(4, 99);(11, 4)]) 3
    |> TUnit.testEq (TUnit.ap3 MapEx.findWithDefault)

    

do  TUnit.newTest "MapEx.insertWith"
    |> TUnit.addCase ((+), 1, 3, Map.ofList [(1, 5); (4, 2)])
                     (Map.ofList [(1, 8); (4, 2)])
    |> TUnit.addCase ((+), 1, 3, Map.ofList [(2, 5); (4, 2)])
                     (Map.ofList [(1, 3); (2, 5); (4, 2)])
    |> TUnit.addCase ((-), 1, 3, Map.ofList [(1, 5); (4, 2)])
                     (Map.ofList [(1, -2); (4, 2)])
    |> TUnit.addCase ((-), 1, 3, Map.ofList [(2, 5); (4, 2)])
                     (Map.ofList [(1, 3); (2, 5); (4, 2)])
    |> TUnit.testEq (TUnit.ap4 MapEx.insertWith)


do  TUnit.newTest "MapEx.adjust"
    |> TUnit.addCase (((+) 9), 1, Map.ofList [(1, 10);(4, 99);(11, 4)])
                     (Map.ofList [(1, 19);(4, 99);(11, 4)])
    |> TUnit.addCase (((+) 9), 1, Map.ofList [(2, 10);(4, 99);(11, 4)])
                     (Map.ofList [(2, 10);(4, 99);(11, 4)])
    |> TUnit.testEq (TUnit.ap3 MapEx.adjust)

    
;;