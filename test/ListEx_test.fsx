//------------------------------------
// Test of List Extensions
//
// version: 1.0
// author : Leonardone @ NEETSDKASU
// license: MIT License
//------------------------------------
#load "TUnit.fsx"
#load "..\ListEx.fs"

open neetsdkasu.utils.Collections


do  TUnit.newTest "ListEx.tail"
    |> TUnit.addCase [1; 2; 3] [2; 3]
    |> TUnit.addCase [2; 3]    [3]
    |> TUnit.addCase [3]       []
    |> TUnit.addCase []        []
    |> TUnit.testEq ListEx.tail
    
    
;;