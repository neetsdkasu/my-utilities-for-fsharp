//------------------------------------
// Test of Seq Extensions
//
// version: 1.0
// author : Leonardone @ NEETSDKASU
// license: MIT License
//------------------------------------
#load "TUnit.fsx"
#load "../SeqEx.fs"

open neetsdkasu.utils.Collections


do  TUnit.newTest "SeqEx.cycle <success case>"
    |> TUnit.addCase [1; 2; 3] [1; 2; 3; 1; 2; 3; 1; 2; 3; 1]
    |> TUnit.addCase [2; 3]    [2; 3; 2; 3; 2; 3; 2; 3; 2; 3]
    |> TUnit.addCase [5]       [5; 5; 5; 5; 5; 5; 5; 5; 5; 5]
    |> TUnit.testEq (fun t ->
        SeqEx.cycle t
        |> Seq.take 10
        |> Seq.toList
        )


do  TUnit.newTest "SeqEx.cycle <failure case>"
    |> TUnit.addCase [] (fun (ex:System.Exception) ->
        match ex with
        | :? System.ArgumentException as ex -> "src" = ex.ParamName
        | _ -> false
        )
    |> TUnit.testEx SeqEx.cycle

    
do  TUnit.newTest "SeqEx.tail"
    |> TUnit.addCase [1; 2; 3] [2; 3]
    |> TUnit.addCase [1; 2]    [2]
    |> TUnit.addCase [1]       []
    |> TUnit.testEq (SeqEx.tail >> Seq.toList)


;;