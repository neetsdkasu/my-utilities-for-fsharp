//------------------------------------
// TUnit
//
// version: 1.0
// author : Leonardone @ NEETSDKASU
// license: MIT License
//------------------------------------


type Tester() =
    inherit System.Object()
    let mutable tests : Map<string, unit -> unit> = Map.empty
    member this.addTest name fn =
        if Map.containsKey name tests then
            do ignore <| tests <- Map.empty
               failwithf "duplicate test name: %s" name
        else
            ignore <| tests <- Map.add name fn tests
    member this.runAllTests () = do
        Map.iter (fun _ fn -> fn()) tests
        ignore <| tests <- Map.empty
    member this.runTest name =
        match Map.tryFind name tests with
        | None     -> printfn "test [%s] is not found." name
        | Some(fn) -> do
            fn()
            ignore <| tests <- Map.remove name tests
    override this.Finalize () = do
        try
            this.runAllTests()
        with
            | ex -> printfn "%s" (ex.ToString())
        base.Finalize()
    

let tester = new Tester()


let ap2 = (<||)
let ap3 = (<|||)
let ap4 fn (a, b, c, d) = fn a b c d
let ap5 fn (a, b, c, d, e) = fn a b c d e
let ap6 fn (a, b, c, d, e, f) = fn a b c d e f


let newTest tn =
    (tn, Seq.empty)


let addCase c r (tn, s) =
    (tn, Seq.append s <| Seq.singleton (c, r))


let private _test tf (tn, ts) =
    tester.addTest tn (fun () ->
        printfn "BEGIN test: %s" tn
        ts
        |> Seq.mapi (fun i t -> (i, t))
        |> Seq.forall tf
        |> function
            | true  -> printfn "END test: %s [All Ok]" tn
            | false -> failwith <| sprintf "Tests failed: %s" tn
    )

let private _ok i =
    printfn " case %2d: Ok" i
    true


let private _ng i c r t =
    printfn " case %2d: %A" i c
    printfn "  expect: %A" r
    printfn "  return: %A" t
    false            

    
let testEq f =
    _test (fun (i, (c, r)) ->
        let t = f c
        if t = r
        then _ok i
        else _ng i c r t
        )


let testEx f =
    _test (fun (i, (c, r)) ->
        try
            let t = f c
            _ng i c () t
        with
            | ex -> if r ex
                    then _ok i
                    else _ng i c () ex
        )


;;