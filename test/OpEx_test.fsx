//------------------------------------
// Test of Operator Extensions
//
// version: 1.0
// author : Leonardone @ NEETSDKASU
// license: MIT License
//------------------------------------
#load "TUnit.fsx"
#load "../OpEx.fs"

open neetsdkasu.utils.Core


do  TUnit.newTest "OpEx.flip"
    |> TUnit.addCase ((-), 3, 5) ((-) 5 3)
    |> TUnit.addCase ((-), 5, 3) ((-) 3 5)
    |> TUnit.testEq (TUnit.ap3 OpEx.flip)


do  TUnit.newTest "OpEx.swap"
    |> TUnit.addCase (1, 2) (2, 1)
    |> TUnit.addCase (5, 3) (3, 5)
    |> TUnit.testEq OpEx.swap


do  TUnit.newTest "OpEx.tuple"
    |> TUnit.addCase (1, 2) (1, 2)
    |> TUnit.addCase (5, 3) (5, 3)
    |> TUnit.testEq (TUnit.ap2 OpEx.tuple)

    
do  TUnit.newTest "OpEx.tuple3"
    |> TUnit.addCase (1, 2, 3) (1, 2, 3)
    |> TUnit.addCase (5, 3, 7) (5, 3, 7)
    |> TUnit.testEq (TUnit.ap3 OpEx.tuple3)


do  TUnit.newTest "OpEx.tuple4"
    |> TUnit.addCase (1, 2, 3, 4) (1, 2, 3, 4)
    |> TUnit.addCase (5, 3, 7, 9) (5, 3, 7, 9)
    |> TUnit.testEq (TUnit.ap4 OpEx.tuple4)


do  TUnit.newTest "OpEx.tuple5"
    |> TUnit.addCase (1, 2, 3, 4, 5) (1, 2, 3, 4, 5)
    |> TUnit.addCase (5, 3, 7, 9, 1) (5, 3, 7, 9, 1)
    |> TUnit.testEq (TUnit.ap5 OpEx.tuple5)

;;