My utilities for F#
===================



```fsharp
namespace neetsdkasu.utils.Collections
  module ListEx = begin
    val tail : 'a list -> 'a list
  end
  module MapEx = begin
    val findWithDefault : 'a -> 'b -> Map<'b,'a> -> 'a when 'b : comparison
    val insertWith :
      ('a -> 'a -> 'a) -> 'b -> 'a -> Map<'b,'a> -> Map<'b,'a>
        when 'b : comparison
    val adjust :
      ('a -> 'a) -> 'b -> Map<'b,'a> -> Map<'b,'a> when 'b : comparison
  end
  module SeqEx = begin
    val cycle : seq<'a> -> seq<'a>
    val tail : seq<'a> -> seq<'a>
  end
namespace neetsdkasu.utils.Core
  module OpEx = begin
    val flip : ('a -> 'b -> 'c) -> 'b -> 'a -> 'c
    val swap : 'a * 'b -> 'b * 'a
    val tuple : 'a -> 'b -> 'a * 'b
    val tuple3 : 'a -> 'b -> 'c -> 'a * 'b * 'c
    val tuple4 : 'a -> 'b -> 'c -> 'd -> 'a * 'b * 'c * 'd
    val tuple5 : 'a -> 'b -> 'c -> 'd -> 'e -> 'a * 'b * 'c * 'd * 'e
  end
```