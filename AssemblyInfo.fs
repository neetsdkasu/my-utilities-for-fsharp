//////////////////////////////////////////////
// AssemblyInfo
//
// author : Leonardone @ NEETSDKASU
//////////////////////////////////////////////
namespace neetsdkasu.utils

module AssemblyInfo =
    open System.Reflection
    open System.Runtime.CompilerServices
    open System.Runtime.InteropServices
    [<assembly: AssemblyTitle("Utilities for F#")>]
    [<assembly: AssemblyDescription("Useful utilities for F#")>]
    [<assembly: AssemblyCompany("Leonardone @ NEETSDKASU")>]
    [<assembly: AssemblyProduct("Utilities for F#")>]
    [<assembly: AssemblyCopyright("(C) 2017 Leonardone @ NEETSDKASU")>]
    [<assembly: ComVisible(false)>]
    [<assembly: Guid("F438A157-7B44-48DC-AD91-333A0EFBBE84")>]
    [<assembly: AssemblyVersion("1.0.0.0")>]
    [<assembly: AssemblyFileVersion("1.0.0.0")>]
    do ()